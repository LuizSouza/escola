package turma;

import java.util.ArrayList;

import pessoa.Aluno;
import pessoa.Professor;

public class Turma{
    private String nome;
    private String serie;
    private Professor professor;
    private ArrayList<Aluno> alunos;

    public Turma(){
        this.nome = new String();
        this.serie = new String();
        this.professor = new Professor();
        this.alunos = new ArrayList<>();
    }

    public String getNome(){
        return this.nome;
    }

    public void setNome(String nome){
        this.nome = nome;
    }

    public String getSerie(){
        return this.serie;
    }

    public void setSerie(String serie){
        this.serie = serie;
    }

    public Professor getProfessor(){
        return this.professor;
    }

    public void setProfessor(Professor professor){
        this.professor = professor;
    }

    public void matricular(Aluno aluno){
        this.alunos.add(aluno);
    }

    // public void cancelarMatricula(Aluno aluno){

    // }

    public void imprimir(){
        System.out.println("----====Dados da Turma====----");
        System.out.println("Nome: " + this.getNome());
        System.out.println("Serie: " + this.getSerie());
        this.professor.imprimir();
        System.out.println("----====Alunos da Turma====----");
        for(int i = 0; i < this.alunos.size(); i++){
            this.alunos.get(i).imprimir();
        }
        System.out.println("----=====================-----");
    }

}