package pessoa;

import java.util.Date;

import turma.Turma;

public class Aluno{
    private String nome;
    private Date dataNascimento;
    private char sexo;
    private Turma turma;

    public Aluno(){
        this.nome = new String();
        this.dataNascimento = new Date();
        this.turma = new Turma();
    }

    public String getNome(){
        return this.nome;
    }

    public void setNome(String nome){
        this.nome = nome;
    }

    public Date getDataNascimento(){
        return this.dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento){
        this.dataNascimento = dataNascimento;
    }

    public String getSexo(){
        if(this.sexo == 'M')
            return "Masculino";
        return "Feminino";
    }

    public void setSexo(char sexo){
        this.sexo = sexo;
    }

    public Turma getTurma(){
        return this.turma;
    }

    public void setTurma(Turma turma){
        this.turma = turma;
    }

    public void imprimir(){
        System.out.println("-----===Dados do Aluno===----");
        System.out.println("Nome:" + this.getNome() );
        System.out.println("Data Nascimento: " + this.getDataNascimento());
        System.out.println("Sexo: " + this.getSexo());
        System.out.println("----=========================-----");
    }
}