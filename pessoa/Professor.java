package pessoa;

import java.util.Date;

public class Professor{
    private String nome;
    private Date dataNascimento;
    private char sexo;
    
    public Professor(){
        this.nome = new String();
        this.dataNascimento = new Date();
        this.sexo = 'A';
    }

    public Professor(String nome){
        this();
        this.nome = nome;
    }

    public Professor(String nome, char sexo){
        this(nome);
        this.sexo = sexo;
    }

    public String getNome(){
        return this.nome;
    }

    public void setNome(String nome){
        this.nome = nome;
    }

    public Date getDataNascimento(){
        return this.dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento){
        this.dataNascimento = dataNascimento;
    }

    public String getSexo(){
        if(this.sexo == 'M')
            return "Masculino";
        if(this.sexo == 'F')
            return "Feminino";
        return "Não Informado!";
    }

    public void setSexo(char sexo){
        this.sexo = sexo;
    }

    public void imprimir(){
        System.out.println("-----===Dados do Professor===----");
        System.out.println("Nome:" + this.getNome() );
        System.out.println("Data Nascimento: " + this.getDataNascimento());
        System.out.println("Sexo: " + this.getSexo());
        System.out.println("----=========================-----");
    }

}