package avaliacao;

import pessoa.Aluno;

public class Nota{

    private Avaliacao avaliacao;
    private Aluno aluno;
    private double nota;

    public Nota(){
        this.avaliacao = new Avaliacao();
        this.aluno = new Aluno();
    }

    public Avaliacao getAvaliacao(){
        return this.avaliacao;
    }

    public void setAvaliacao(Avaliacao avaliacao){
        this.avaliacao = avaliacao;
    }

    public Aluno getAluno(){
        return this.aluno;
    }

    public void setAluno(Aluno aluno){
        this.aluno = aluno;
    }

    public double getNota(){
        return this.nota;
    }

    public void setNota(double nota){
        this.nota = nota;
    }
    
}