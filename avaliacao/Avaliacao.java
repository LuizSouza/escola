package avaliacao;

import java.util.Date;
import turma.Turma;

public class Avaliacao{

    private String disciplina;
    private int valor;
    private String tipo;
    private Date data;
    private Turma turma;

    public Avaliacao(){
        this.disciplina = new String();
        this.valor = 0;
        this.tipo = new String();
        this.data = new Date();
        this.turma = new Turma();
    }

    public String getDisciplina(){
        return this.disciplina;
    }

    public void setDisciplina(String disciplina){
        this.disciplina = disciplina;
    }

    public int getValor(){
        return this.valor;
    }

    public void setValor(int valor){
        this.valor = valor;
    }

    public String getTipo(){
        return this.tipo;
    }

    public void setTipo(String tipo){
        this.tipo = tipo;
    }

    public Turma getTurma(){
        return this.turma;
    }

    public void setTurma(Turma turma){
        this.turma = turma;
    }

}