import pessoa.Aluno;
import pessoa.Professor;
import turma.Turma;

import java.util.Date;

import avaliacao.Avaliacao;
import avaliacao.Nota;

public class Escola{
    public static void main(String args[]){
        Professor professor = new Professor();
        professor.setNome("Yvssa");
        professor.setDataNascimento(new Date(85, 8, 25));
        professor.setSexo('F');

        Professor p2 = new Professor("Wilbert");

        Professor p3 = new Professor("Amaury", 'M');

        // professor.imprimir();
        // p2.imprimir();
        // p3.imprimir();


        

        Turma turma = new Turma();
        turma.setNome("A");
        turma.setSerie("6º Ano");
        turma.setProfessor(professor);

        // turma.imprimir();

        professor.setNome("Yvssa Carneiro");
        // turma.getProfessor().setNome("Yvssa Carneiro");


        // professor.imprimir();

        Aluno aluno = new Aluno();
        aluno.setNome("Joanina");
        aluno.setDataNascimento(new Date(98, 5, 20));
        aluno.setSexo('F');
        aluno.setTurma(turma);

        turma.matricular(aluno);

        aluno = new Aluno();
        aluno.setNome("Raquel");
        aluno.setDataNascimento(new Date(88, 5, 21));
        aluno.setSexo('F');
        aluno.setTurma(turma);

        turma.matricular(aluno);

        turma.imprimir();

        // Avaliacao avaliacao = new Avaliacao();
        // avaliacao.setDisciplina("Matematica");
        // avaliacao.setTipo("Avaliação");
        // avaliacao.setTurma(turma);
        // avaliacao.setValor(10);

        // Nota nota = new Nota();
        // nota.setAluno(aluno);
        // nota.setAvaliacao(avaliacao);
        // nota.setNota(9.6);

    }
}